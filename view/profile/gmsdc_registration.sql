--
-- PostgreSQL database dump
--

-- Dumped from database version 12.1
-- Dumped by pg_dump version 12.1

-- Started on 2020-04-02 11:57:55

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

SET default_tablespace = '';

SET default_table_access_method = heap;

--
-- TOC entry 202 (class 1259 OID 32859)
-- Name: gmsdc_registration; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.gmsdc_registration (
    id bigint,
    name character(255),
    datejoin character(255)
);


ALTER TABLE public.gmsdc_registration OWNER TO postgres;

--
-- TOC entry 2824 (class 0 OID 32859)
-- Dependencies: 202
-- Data for Name: gmsdc_registration; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.gmsdc_registration (id, name, datejoin) FROM stdin;
1	Steven Zhang                                                                                                                                                                                                                                                   	Joined in January 2020                                                                                                                                                                                                                                         
\.


-- Completed on 2020-04-02 11:57:55

--
-- PostgreSQL database dump complete
--


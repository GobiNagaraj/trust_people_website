--
-- PostgreSQL database dump
--

-- Dumped from database version 12.1
-- Dumped by pg_dump version 12.1

-- Started on 2020-04-02 11:58:10

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

SET default_tablespace = '';

SET default_table_access_method = heap;

--
-- TOC entry 206 (class 1259 OID 41037)
-- Name: image_upload; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.image_upload (
    image_id integer NOT NULL,
    image_name character(200) NOT NULL
);


ALTER TABLE public.image_upload OWNER TO postgres;

--
-- TOC entry 205 (class 1259 OID 41035)
-- Name: image_upload_image_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.image_upload_image_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.image_upload_image_id_seq OWNER TO postgres;

--
-- TOC entry 2832 (class 0 OID 0)
-- Dependencies: 205
-- Name: image_upload_image_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.image_upload_image_id_seq OWNED BY public.image_upload.image_id;


--
-- TOC entry 2698 (class 2604 OID 41040)
-- Name: image_upload image_id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.image_upload ALTER COLUMN image_id SET DEFAULT nextval('public.image_upload_image_id_seq'::regclass);


--
-- TOC entry 2826 (class 0 OID 41037)
-- Dependencies: 206
-- Data for Name: image_upload; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.image_upload (image_id, image_name) FROM stdin;
1	steven.jfif                                                                                                                                                                                             
2	                                                                                                                                                                                                        
3	photo (1).jpg                                                                                                                                                                                           
4	                                                                                                                                                                                                        
5	                                                                                                                                                                                                        
6	                                                                                                                                                                                                        
7	leslie_the_dog.jpg                                                                                                                                                                                      
\.


--
-- TOC entry 2833 (class 0 OID 0)
-- Dependencies: 205
-- Name: image_upload_image_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.image_upload_image_id_seq', 7, true);


-- Completed on 2020-04-02 11:58:10

--
-- PostgreSQL database dump complete
--


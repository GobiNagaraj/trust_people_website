--
-- PostgreSQL database dump
--

-- Dumped from database version 12.1
-- Dumped by pg_dump version 12.1

-- Started on 2020-04-02 11:58:27

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

SET default_tablespace = '';

SET default_table_access_method = heap;

--
-- TOC entry 204 (class 1259 OID 32874)
-- Name: gmsdc_profile_stats; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.gmsdc_profile_stats (
    bio character(500),
    post_number bigint,
    followers bigint,
    following bigint,
    profile_id bigint,
    profile_name character(255),
    degree character(255),
    study character(255),
    grade character(255),
    description character(255),
    current_password character(20),
    new_password character(20),
    other_info character(255)
);


ALTER TABLE public.gmsdc_profile_stats OWNER TO postgres;

--
-- TOC entry 2824 (class 0 OID 32874)
-- Dependencies: 204
-- Data for Name: gmsdc_profile_stats; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.gmsdc_profile_stats (bio, post_number, followers, following, profile_id, profile_name, degree, study, grade, description, current_password, new_password, other_info) FROM stdin;
Hello Guys                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                          	100	100	100	1	Steven Zhang                                                                                                                                                                                                                                                   	Computer Science                                                                                                                                                                                                                                               	Webull/Day-Trading                                                                                                                                                                                                                                             	Up 150 bucks in the market                                                                                                                                                                                                                                     	Whoops no one saw that                                                                                                                                                                                                                                         	123456              	123456              	This is my other info                                                                                                                                                                                                                                          
\.


-- Completed on 2020-04-02 11:58:28

--
-- PostgreSQL database dump complete
--


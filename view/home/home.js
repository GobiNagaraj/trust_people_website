Vue.component("modal", {
    template: "#modal-template"
  });

var app = new Vue({
    el: '#app',
    data: {
  errorMsg : "",
        successMsg : "",
  users : [],
  showModal: false,
  showModal2: false,
  agreeVal: 0,
  similarVal: 0,
  posts : [],
        newPost : {name: "", date_post: "", role: "", date_discussed: "", location: "", attachment: "", profile_picture: "", comment:""},
        currentPost : {},
        // a : 1
    },
    mounted: function(){
        
        this.getAllPost();
        // this.a+1;
    },
methods: {
        
        getAllPost(){
            axios.get("http://localhost/trust_people/model.php?action=readpost").then(function(response){
                if(response.data.error){
                    app.errorMsg = response.data.message;
                }
                else{
                    app.posts = response.data.posts;
                }
            });
        },

  updateFollower(){
            axios.get("http://localhost/trust_people/model.php?action=updatefollower").then(function(response){
                if(response.data.error){
                    app.errorMsg = response.data.message;
                }
                else{
                    app.posts = response.data.posts;
                }
            });
    alert('You are now following this user.');
        },

  addComment(){
                var formData = app.toFormData(app.newPost);
                axios.post("http://localhost/trust_people/process.php?action=createcom", formData).then(function
                (response){
                    app.newPost = {comment: ""};
                    if(response.data.error){
                        app.errorMsg = response.data.message;
                    }
                    else{
                        app.successMsg = response.data.message;
                        app.getAllPost();
                    }
                });
        },

  updateUser(){
            var formData = app.toFormData(app.currentProfile);
            axios.post("http://localhost/trust_people/model.php?action=update_profile", formData).then(function(response){
                app.currentProfile = {};
                if(response.data.error){
                    app.errorMsg = response.data.message;
                }
                else{
                    app.successMsg = response.data.message;
                    app.getAllProfile();
                }
            });
  },
  incrementAgree() {
  this.agreeVal = this.agreeVal + 1;
},

incrementSimilar() {
  this.similarVal = this.similarVal + 1;
},
        
        toFormData(obj){
            var fd = new FormData();
            for(var i in obj){
                fd.append(i, obj[i]);
            }
            return fd;
        },
        selectProfile(profile){
            app.currentProfile = profile;
        },
        clearMsg(){
            app.errorMsg = "";
            app.successMsg = "";
        }
        },
});
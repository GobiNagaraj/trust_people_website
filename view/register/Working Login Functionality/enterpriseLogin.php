<!DOCTYPE html>
<html>
<head>
	<title>Trust People Home Page</title>
	<meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
	<link rel="stylesheet" type="text/css" href="../../assets/css/bootstrap.min.css">
	<script src="../../assets/js/jquery.min.js"></script>
	<script src="../../assets/js/bootstrap.min.js"></script>

	<link href="../../assets/css/font-awesome.css" rel="stylesheet" type="text/css" />
	<link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,300italic,regular,italic,600,700,700italic" rel="stylesheet">
	<!-- custom css -->
	<link rel="stylesheet" type="text/css" href="../../assets/css/custom.css">
	<link rel="stylesheet" type="text/css" href="../../assets/css/register.css">
</head>
<body>
<div id="app">
	<div class="container">
		<div class="col-md-12 homeDiv">
			<div class="col-md-6 left">
				<img src="../../assets/images/logo.png">
			</div>
			<div class="col-md-6 right">
				<div class="col-md-3">
					<a href="#" class="homeLbl">For Enterprise</a>	
				</div>
				<div class="col-md-1">
					<label>|</label>	
				</div>
				<div class="col-md-3">
					<a href="signIn.php" class="homeLbl">Sign In</a>	
				</div>
				<div class="col-md-3">
					<a href="signIn.php" class="registerBtn">Register</a>
				</div>
			</div>
		</div>
		<hr>
		<div class="main">
			<div class="form_wrapper">
			  	<div class="form_container">
				    <div class="title_container">
				      <h3>Create your Enterprise account</h3>
				      <h2>Sign up for TrustPeople Enterprise account</h2>
				    </div>
				    <div class="row clearfix">
				      <div class="">
				        <form action="#" method="POST"> 
				          	<div class="row clearfix">
					            <div class="col_half">
					              <div class="input_field">
					                <input type="text" class="form-control inputElement" name="name" placeholder="First Name" v-model="newUser.firstname"/>
					              </div>
					            </div>
					            <div class="col_half">
					              <div class="input_field">
					                <input type="text" class="form-control inputElement" name="name" placeholder="Last Name" v-model="newUser.lastname" required />
					              </div>
					            </div>
				          	</div>
				          	<div class="row clearfix">
					            <div class="col_half">
					              <div class="input_field">
					                <input type="text" class="form-control inputElement" name="name" placeholder="Title" v-model="newUser.title"/>
					              </div>
					            </div>
					            <div class="col_half">
					              <div class="input_field">
					                <input type="text" class="form-control inputElement" name="name" placeholder="Company Email" v-model="newUser.companyemail" required />
					              </div>
					            </div>
				          	</div>
				          	<div class="row clearfix">
					            <div class="col_half">
					              <div class="input_field">
					                <input type="text" class="form-control inputElement" name="name" placeholder="Phone Number" v-model="newUser.phone"/>
					              </div>
					            </div>
					            <div class="col_half">
					              <div class="input_field">
					                <input type="text" class="form-control inputElement" name="name" placeholder="Company Name" v-model="newUser.companyname" required />
					              </div>
					            </div>
				          	</div>
				          	<div class="row">
					            <div class="input_field">
					                <input type="text" class="form-control inputElement" name="name" placeholder="Company Website" v-model="newUser.companywebsite" required />
  				                </div>
				          	</div>
				          	<p>By clicking Sign In, you agree to our <strong>Terms of Use</strong> and our <strong>Privacy Policy.</strong></p>
				          	<input class="button" type="submit" value="SIGN UP" @click="clearMsg(); addUser();"/>
				        </form>
				      </div>
				    </div>
			  	</div>
			</div>
		</div>
	</div>
</div>

<!-- Javascript Imports -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/axios/0.19.0/axios.min.js"></script>
<script src="https://cdn.jsdelivr.net/npm/vue"></script>
<!--VUE APP-->
<!-- <script>
	//vue
	var app = new Vue({
		el: '#app',
		data: {
		errorMsg : "",
		successMsg : "",
      	users : [],
		//init
		newUser : {firstname: "", lastname: "", title: "", companyemail: "", phone: "", companyname: "", companywebsite: ""},
      	currentUser : {},
		},
		methods: {
			//add user
			addUser(){
        var formData = app.toFormData(app.newUser);
				//reference model.php
				axios.post("http://localhost/register/model.php?action=create", formData).then(function(response){
					//error
					if(response.data.error){
						app.errorMsg = response.data.message;
					}
					else{
						app.successMsg = response.data.message;
					}
				});
			},    
			//form data
			toFormData(obj){
				var fd = new FormData();
				for(var i in obj){
					fd.append(i, obj[i]);
				}
				return fd;
			},
			//clear message
			clearMsg(){
				app.errorMsg = "";
				app.successMsg = "";
			}
		}
	});
</script> -->
<?php
		include_once("config.php");
         if(isset($_POST["submit"])){
            // Create connection
			$conn = mysqli_connect("localhost","root","","disqus_command");
			
            // Check connection
            if ($conn->connect_error) {
               die("Connection failed: " . $conn->connect_error);
            } 
            $sql = "INSERT INTO userbd(username password)VALUES ('".$_POST["username"].", ".$_POST["password"]."')";

            if (mysqli_query($conn, $sql)) {
               echo "New record created successfully";
            } else {
               echo "Error: " . $sql . "" . mysqli_error($conn);
            }
            $conn->close();
         }
      ?>
</body>
</html>